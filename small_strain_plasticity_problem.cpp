/** \file small_strain_plasticity.cpp
 * \ingroup small_strain_plasticity
 * \brief Small strain plasticity example
 *
 */

/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */


#include <MoFEM.hpp>
using namespace MoFEM;

#include <boost/program_options.hpp>
using namespace std;
namespace po = boost::program_options;

#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/symmetric.hpp>

#include <adolc/adolc.h>
#include <SmallStrainPlasticity.hpp>
#include <SmallStrainPlasticityMaterialModels.hpp>

#include <MethodForForceScaling.hpp>
#include <TimeForceScale.hpp>
#include <SurfacePressure.hpp>
#include <NodalForce.hpp>
#include <EdgeForce.hpp>
#include <DirichletBC.hpp>

#include <Projection10NodeCoordsOnField.hpp>
#include <PostProcOnRefMesh.hpp>

#include <string>

using namespace boost::numeric;




static char help[] =
"...\n"
"\n";

int main(int argc, char *argv[]) {
  PetscInitialize(&argc,&argv,(char *)0,help);

  moab::Core mb_instance;
  moab::Interface& moab = mb_instance;
  ParallelComm* pcomm = ParallelComm::get_pcomm(&moab,MYPCOMM_INDEX);
  if(pcomm == NULL) pcomm =  new ParallelComm(&moab,PETSC_COMM_WORLD);

  PetscBool flg = PETSC_TRUE;
  char mesh_file_name[255];
  ierr = PetscOptionsGetString(PETSC_NULL,"-my_file",mesh_file_name,255,&flg); CHKERRQ(ierr);
  if(flg != PETSC_TRUE) {
    SETERRQ(PETSC_COMM_SELF,1,"*** ERROR -my_file (MESH FILE NEEDED)");
  }

  PetscInt order;
  ierr = PetscOptionsGetInt(PETSC_NULL,"-my_order",&order,&flg); CHKERRQ(ierr);
  if(flg != PETSC_TRUE) {
    order = 2;
  }
  PetscInt bubble_order;
  ierr = PetscOptionsGetInt(PETSC_NULL,"-my_bubble_order",&bubble_order,&flg); CHKERRQ(ierr);
  if(flg != PETSC_TRUE) {
    bubble_order = order;
  }

  // use this if your mesh is partitioned and you run code on parts,
  // you can solve very big problems
  PetscBool is_partitioned = PETSC_FALSE;
  ierr = PetscOptionsGetBool(PETSC_NULL,"-my_is_partitioned",&is_partitioned,&flg); CHKERRQ(ierr);

  //Read mesh to MOAB
  if(is_partitioned == PETSC_TRUE) {
    const char *option;
    option = "PARALLEL=BCAST_DELETE;PARALLEL_RESOLVE_SHARED_ENTS;PARTITION=PARALLEL_PARTITION;";
    rval = moab.load_file(mesh_file_name, 0, option); CHKERRQ_MOAB(rval);
  } else {
    const char *option;
    option = "";
    rval = moab.load_file(mesh_file_name, 0, option); CHKERRQ_MOAB(rval);
  }

  SmallStrainJ2Plasticity cp;
  {
    cp.tAgs.resize(3);
    cp.tAgs[0] = 0;
    cp.tAgs[1] = 1;
    cp.tAgs[2] = 2;
    cp.tOl = 1e-12;

    double young_modulus = 1;
    double poisson_ratio = 0.25;
    cp.sIgma_y = 1;
    cp.H = 0.1;
    cp.K = 0;
    cp.phi = 1;
    {
      ierr = PetscOptionsGetReal(0,"-my_young_modulus",&young_modulus,0); CHKERRQ(ierr);
      ierr = PetscOptionsGetReal(0,"-my_poisson_ratio",&poisson_ratio,0); CHKERRQ(ierr);
      cp.mu = MU(young_modulus,poisson_ratio);
      cp.lambda = LAMBDA(young_modulus,poisson_ratio);
      ierr = PetscOptionsGetReal(0,"-my_sigma_y",&cp.sIgma_y,0); CHKERRQ(ierr);
      ierr = PetscOptionsGetReal(0,"-my_H",&cp.H,0); CHKERRQ(ierr);
      ierr = PetscOptionsGetReal(0,"-my_K",&cp.K,0); CHKERRQ(ierr);
      ierr = PetscOptionsGetReal(0,"-my_phi",&cp.phi,0); CHKERRQ(ierr);
    }

    PetscPrintf(PETSC_COMM_WORLD,"young_modulus = %4.2e \n",young_modulus);
    PetscPrintf(PETSC_COMM_WORLD,"poisson_ratio = %4.2e \n",poisson_ratio);
    PetscPrintf(PETSC_COMM_WORLD,"sIgma_y = %4.2e \n",cp.sIgma_y);
    PetscPrintf(PETSC_COMM_WORLD,"H = %4.2e \n",cp.H);
    PetscPrintf(PETSC_COMM_WORLD,"K = %4.2e \n",cp.K);
    PetscPrintf(PETSC_COMM_WORLD,"phi = %4.2e \n",cp.phi);


    cp.sTrain.resize(6,false);
    cp.sTrain.clear();
    cp.plasticStrain.resize(6,false);
    cp.plasticStrain.clear();
    cp.internalVariables.resize(7,false);
    cp.internalVariables.clear();
    cp.createMatAVecR();
    cp.snesCreate();
    // ierr = SNESSetFromOptions(cp.sNes); CHKERRQ(ierr);

  }

  MoFEM::Core core(moab);
  MoFEM::Interface& m_field = core;

  //ref meshset ref level 0
  ierr = m_field.seed_ref_level_3D(0,BitRefLevel().set(0)); CHKERRQ(ierr);
  vector<BitRefLevel> bit_levels;
  bit_levels.push_back(BitRefLevel().set(0));

  {

    BitRefLevel problem_bit_level = bit_levels.back();
    Range CubitSideSets_meshsets;
    ierr = m_field.get_cubit_meshsets(SIDESET,CubitSideSets_meshsets); CHKERRQ(ierr);

    //Fields
    ierr = m_field.add_field("DISPLACEMENT",H1,AINSWORTH_LEGENDRE_BASE,3); CHKERRQ(ierr);
    ierr = m_field.add_field("MESH_NODE_POSITIONS",H1,AINSWORTH_LEGENDRE_BASE,3); CHKERRQ(ierr);
    //add entitities (by tets) to the field
    ierr = m_field.add_ents_to_field_by_type(0,MBTET,"DISPLACEMENT"); CHKERRQ(ierr);
    ierr = m_field.add_ents_to_field_by_type(0,MBTET,"MESH_NODE_POSITIONS"); CHKERRQ(ierr);

    //set app. order
    ierr = m_field.set_field_order(0,MBTET,"DISPLACEMENT",bubble_order); CHKERRQ(ierr);
    ierr = m_field.set_field_order(0,MBTRI,"DISPLACEMENT",order); CHKERRQ(ierr);
    ierr = m_field.set_field_order(0,MBEDGE,"DISPLACEMENT",order); CHKERRQ(ierr);
    ierr = m_field.set_field_order(0,MBVERTEX,"DISPLACEMENT",1); CHKERRQ(ierr);

    ierr = m_field.set_field_order(0,MBTET,"MESH_NODE_POSITIONS",order>1 ? 2 : 1); CHKERRQ(ierr);
    ierr = m_field.set_field_order(0,MBTRI,"MESH_NODE_POSITIONS",order>1 ? 2 : 1); CHKERRQ(ierr);
    ierr = m_field.set_field_order(0,MBEDGE,"MESH_NODE_POSITIONS",order>1 ? 2 : 1); CHKERRQ(ierr);
    ierr = m_field.set_field_order(0,MBVERTEX,"MESH_NODE_POSITIONS",1); CHKERRQ(ierr);

    //build field
    ierr = m_field.build_fields(); CHKERRQ(ierr);
    {
      //10 node tets
      Projection10NodeCoordsOnField ent_method_material(m_field,"MESH_NODE_POSITIONS");
      ierr = m_field.loop_dofs("MESH_NODE_POSITIONS",ent_method_material,0); CHKERRQ(ierr);
    }

    //FE
    ierr = m_field.add_finite_element("PLASTIC"); CHKERRQ(ierr);
    //Define rows/cols and element data
    ierr = m_field.modify_finite_element_add_field_row("PLASTIC","DISPLACEMENT"); CHKERRQ(ierr);
    ierr = m_field.modify_finite_element_add_field_col("PLASTIC","DISPLACEMENT"); CHKERRQ(ierr);
    ierr = m_field.modify_finite_element_add_field_data("PLASTIC","DISPLACEMENT"); CHKERRQ(ierr);
    ierr = m_field.modify_finite_element_add_field_data("PLASTIC","MESH_NODE_POSITIONS"); CHKERRQ(ierr);
    ierr = m_field.add_ents_to_finite_element_by_type(0,MBTET,"PLASTIC"); CHKERRQ(ierr);

    // Add Neumann forces
    ierr = MetaNeummanForces::addNeumannBCElements(m_field,"DISPLACEMENT"); CHKERRQ(ierr);
    ierr = MetaNodalForces::addElement(m_field,"DISPLACEMENT"); CHKERRQ(ierr);
    ierr = MetaEdgeForces::addElement(m_field,"DISPLACEMENT"); CHKERRQ(ierr);

    //build finite elements
    ierr = m_field.build_finite_elements(); CHKERRQ(ierr);
    //build adjacencies
    ierr = m_field.build_adjacencies(problem_bit_level); CHKERRQ(ierr);

    DMType dm_name = "PLASTIC_PROB";
    ierr = DMRegister_MoFEM(dm_name); CHKERRQ(ierr);
    //craete dm instance
    DM dm;
    ierr = DMCreate(PETSC_COMM_WORLD,&dm);CHKERRQ(ierr);
    ierr = DMSetType(dm,dm_name);CHKERRQ(ierr);
    {
      //set dm datastruture which created mofem datastructures
      ierr = DMMoFEMCreateMoFEM(dm,&m_field,dm_name,problem_bit_level); CHKERRQ(ierr);
      ierr = DMSetFromOptions(dm); CHKERRQ(ierr);
      ierr = DMMoFEMSetIsPartitioned(dm,is_partitioned); CHKERRQ(ierr);
      //add elements to dm
      ierr = DMMoFEMAddElement(dm,"PLASTIC"); CHKERRQ(ierr);
      ierr = DMMoFEMAddElement(dm,"FORCE_FE"); CHKERRQ(ierr);
      ierr = DMMoFEMAddElement(dm,"PRESSURE_FE"); CHKERRQ(ierr);
      ierr = DMSetUp(dm); CHKERRQ(ierr);
    }

    //create matrices
    Vec F,D;
    Mat Aij;
    {
      ierr = DMCreateGlobalVector_MoFEM(dm,&D); CHKERRQ(ierr);
      ierr = VecDuplicate(D,&F); CHKERRQ(ierr);
      ierr = DMCreateMatrix_MoFEM(dm,&Aij); CHKERRQ(ierr);
      ierr = VecZeroEntries(D); CHKERRQ(ierr);
      ierr = VecGhostUpdateBegin(D,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      ierr = VecGhostUpdateEnd(D,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
      ierr = DMoFEMMeshToLocalVector(dm,D,INSERT_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);
      ierr = MatZeroEntries(Aij); CHKERRQ(ierr);
    }

    //assemble Aij and F
    DisplacementBCFEMethodPreAndPostProc dirichlet_bc(m_field,"DISPLACEMENT",Aij,D,F);
    dirichlet_bc.methodsOp.push_back(new TimeForceScale("-my_displacements_history",false));

    SmallStrainPlasticity small_strain_plasticity(m_field);
    {
      PetscBool bbar = PETSC_TRUE;
      ierr = PetscOptionsGetBool(0,"-my_bbar",&bbar,0); CHKERRQ(ierr);
      small_strain_plasticity.commonData.bBar = bbar;
    }
    {
      small_strain_plasticity.feRhs.getOpPtrVector().push_back(
        new SmallStrainPlasticity::OpGetCommonDataAtGaussPts(
          "DISPLACEMENT",small_strain_plasticity.commonData
        )
      );
      small_strain_plasticity.feRhs.getOpPtrVector().push_back(
        new SmallStrainPlasticity::OpCalculateStress(
          m_field,"DISPLACEMENT",small_strain_plasticity.commonData,cp
        )
      );
      small_strain_plasticity.feRhs.getOpPtrVector().push_back(
        new SmallStrainPlasticity::OpAssembleRhs(
          "DISPLACEMENT",small_strain_plasticity.commonData
        )
      );
      small_strain_plasticity.feLhs.getOpPtrVector().push_back(
        new SmallStrainPlasticity::OpGetCommonDataAtGaussPts(
          "DISPLACEMENT",small_strain_plasticity.commonData
        )
      );
      small_strain_plasticity.feLhs.getOpPtrVector().push_back(
        new SmallStrainPlasticity::OpCalculateStress(
          m_field,"DISPLACEMENT",small_strain_plasticity.commonData,cp
        )
      );
      small_strain_plasticity.feLhs.getOpPtrVector().push_back(
        new SmallStrainPlasticity::OpAssembleLhs(
          "DISPLACEMENT",small_strain_plasticity.commonData
        )
      );
      small_strain_plasticity.feUpdate.getOpPtrVector().push_back(
        new SmallStrainPlasticity::OpGetCommonDataAtGaussPts(
          "DISPLACEMENT",small_strain_plasticity.commonData
        )
      );
      small_strain_plasticity.feUpdate.getOpPtrVector().push_back(
        new SmallStrainPlasticity::OpCalculateStress(
          m_field,"DISPLACEMENT",small_strain_plasticity.commonData,cp
        )
      );
      small_strain_plasticity.feUpdate.getOpPtrVector().push_back(
        new SmallStrainPlasticity::OpUpdate(
          "DISPLACEMENT",small_strain_plasticity.commonData
        )
      );
      ierr = small_strain_plasticity.createInternalVariablesTag(); CHKERRQ(ierr);
    }

    // Setting finite element method for applying tractions
    boost::ptr_map<string,NeummanForcesSurface> neumann_forces;
    boost::ptr_map<string,NodalForce> nodal_forces;
    boost::ptr_map<string,EdgeForce> edge_forces;
    TimeForceScale time_force_scale("-my_load_history",false);
    {
      //forces and pressures on surface
      ierr = MetaNeummanForces::setMomentumFluxOperators(
        m_field,neumann_forces,PETSC_NULL,"DISPLACEMENT"
      ); CHKERRQ(ierr);
      //noadl forces
      ierr = MetaNodalForces::setOperators(m_field,nodal_forces,PETSC_NULL,"DISPLACEMENT"); CHKERRQ(ierr);
      //edge forces
      ierr = MetaEdgeForces::setOperators(m_field,edge_forces,PETSC_NULL,"DISPLACEMENT"); CHKERRQ(ierr);
      for(
        boost::ptr_map<string,NeummanForcesSurface>::iterator mit = neumann_forces.begin();
        mit!=neumann_forces.end();mit++
      ) {
        mit->second->methodsOp.push_back(new TimeForceScale("-my_load_history",false));
      }
      for(
        boost::ptr_map<string,NodalForce>::iterator mit = nodal_forces.begin();
        mit!=nodal_forces.end();mit++
      ) {
        mit->second->methodsOp.push_back(new TimeForceScale("-my_load_history",false));
      }
      for(
        boost::ptr_map<string,EdgeForce>::iterator mit = edge_forces.begin();
        mit!=edge_forces.end();mit++
      ) {
        mit->second->methodsOp.push_back(new TimeForceScale("-my_load_history",false));
      }
    }

    //Adding elements to DMSnes
    {
      //Rhs
      ierr = DMMoFEMSNESSetFunction(dm,DM_NO_ELEMENT,NULL,&dirichlet_bc,NULL); CHKERRQ(ierr);
      {
        boost::ptr_map<string,NeummanForcesSurface>::iterator fit;
        fit = neumann_forces.begin();
        for(;fit!=neumann_forces.end();fit++) {
          ierr = DMMoFEMSNESSetFunction(dm,fit->first.c_str(),&fit->second->getLoopFe(),NULL,NULL); CHKERRQ(ierr);
        }
        fit = nodal_forces.begin();
        for(;fit!=nodal_forces.end();fit++) {
          ierr = DMMoFEMSNESSetFunction(dm,fit->first.c_str(),&fit->second->getLoopFe(),NULL,NULL); CHKERRQ(ierr);
        }
        fit = edge_forces.begin();
        for(;fit!=edge_forces.end();fit++) {
          ierr = DMMoFEMSNESSetFunction(dm,fit->first.c_str(),&fit->second->getLoopFe(),NULL,NULL); CHKERRQ(ierr);
        }
      }
      ierr = DMMoFEMSNESSetFunction(dm,"PLASTIC",&small_strain_plasticity.feRhs,PETSC_NULL,PETSC_NULL); CHKERRQ(ierr);
      ierr = DMMoFEMSNESSetFunction(dm,DM_NO_ELEMENT,NULL,NULL,&dirichlet_bc); CHKERRQ(ierr);

      //Lhs
      ierr = DMMoFEMSNESSetJacobian(dm,DM_NO_ELEMENT,NULL,&dirichlet_bc,NULL); CHKERRQ(ierr);
      ierr = DMMoFEMSNESSetJacobian(dm,"PLASTIC",&small_strain_plasticity.feLhs,NULL,NULL); CHKERRQ(ierr);
      ierr = DMMoFEMSNESSetJacobian(dm,DM_NO_ELEMENT,NULL,NULL,&dirichlet_bc); CHKERRQ(ierr);
    }

    // Create Time Stepping solver
    SNES snes;
    SnesCtx *snes_ctx;
    {
      ierr = SNESCreate(PETSC_COMM_WORLD,&snes); CHKERRQ(ierr);
      //ierr = SNESSetDM(snes,dm); CHKERRQ(ierr);
      ierr = DMMoFEMGetSnesCtx(dm,&snes_ctx); CHKERRQ(ierr);
      ierr = SNESSetFunction(snes,F,SnesRhs,snes_ctx); CHKERRQ(ierr);
      ierr = SNESSetJacobian(snes,Aij,Aij,SnesMat,snes_ctx); CHKERRQ(ierr);
      ierr = SNESSetFromOptions(snes); CHKERRQ(ierr);
    }

    PostProcVolumeOnRefinedMesh post_proc(m_field);
    {
      ierr = post_proc.generateReferenceElementMesh(); CHKERRQ(ierr);
      ierr = post_proc.addFieldValuesPostProc("DISPLACEMENT"); CHKERRQ(ierr);
      ierr = post_proc.addFieldValuesGradientPostProc("DISPLACEMENT"); CHKERRQ(ierr);
      ierr = post_proc.addFieldValuesPostProc("MESH_NODE_POSITIONS"); CHKERRQ(ierr);
    }

    {
      double final_time = 1,delta_time = 0.1;
      ierr = PetscOptionsGetReal(0,"-my_final_time",&final_time,0); CHKERRQ(ierr);
      ierr = PetscOptionsGetReal(0,"-my_delta_time",&delta_time,0); CHKERRQ(ierr);
      double delta_time0 = delta_time;

      // ierr = MatView(Aij,PETSC_VIEWER_DRAW_SELF); CHKERRQ(ierr);
      // std::string wait;
      // std::cin >> wait;

      Vec D0;
      ierr = VecDuplicate(D,&D0); CHKERRQ(ierr);

      NumeredDofEntity_multiIndex_uid_view_ordered load_path_dofs_view;
      {
        Range node_set;
        for(_IT_CUBITMESHSETS_BY_NAME_FOR_LOOP_(m_field,"LoadPath",cit)) {
          EntityHandle meshset = cit->getMeshset();
          Range nodes;
          rval = moab.get_entities_by_type(meshset,MBVERTEX,nodes,true); CHKERRQ_MOAB(rval);
          node_set.merge(nodes);
        }
        PetscPrintf(PETSC_COMM_WORLD,"Nb. nodes in load path: %u\n",node_set.size());
        const Problem *problem_ptr;
        ierr = m_field.get_problem("PLASTIC_PROB",&problem_ptr); CHKERRQ(ierr);
        boost::shared_ptr<NumeredDofEntity_multiIndex> numered_dofs_rows = problem_ptr->getNumeredDofsRows();
        Range::iterator nit = node_set.begin();
        for(;nit!=node_set.end();nit++) {
          NumeredDofEntityByEnt::iterator dit,hi_dit;
          dit = numered_dofs_rows->get<Ent_mi_tag>().lower_bound(*nit);
          hi_dit = numered_dofs_rows->get<Ent_mi_tag>().upper_bound(*nit);
          for(;dit!=hi_dit;dit++) {
            load_path_dofs_view.insert(*dit);
          }
        }
      }

      int step = 0;
      double t = 0;
      SNESConvergedReason reason = SNES_CONVERGED_ITERATING;
      for(;t<final_time;step++) {

        t += delta_time;
        PetscPrintf(
          PETSC_COMM_WORLD,"Step %d Time %6.4g final time %3.2g\n",step,t,final_time
        );

        //set time
        {
          dirichlet_bc.ts_t = t;
          {
            boost::ptr_map<string,NeummanForcesSurface>::iterator fit;
            fit = neumann_forces.begin();
            for(;fit!=neumann_forces.end();fit++) {
              fit->second->getLoopFe().ts_t = t;
            }
            fit = nodal_forces.begin();
            for(;fit!=nodal_forces.end();fit++) {
              fit->second->getLoopFe().ts_t = t;
            }
            fit = edge_forces.begin();
            for(;fit!=edge_forces.end();fit++) {
              fit->second->getLoopFe().ts_t = t;
            }
          }
        }

        //solve problem at step
        {
          // dirichlet_bc.snes_B = Aij;
          // small_strain_plasticity.feLhs.snes_B = Aij;
          ierr = VecAssemblyBegin(D);
          ierr = VecAssemblyEnd(D);
          ierr = VecCopy(D,D0); CHKERRQ(ierr);
          if(step == 0 || reason < 0) {
            ierr = SNESSetLagJacobian(snes,-2); CHKERRQ(ierr);
          } else {
            ierr = SNESSetLagJacobian(snes,-1); CHKERRQ(ierr);
          }
          ierr = SNESSolve(snes,PETSC_NULL,D); CHKERRQ(ierr);

          int its;
          ierr = SNESGetIterationNumber(snes,&its); CHKERRQ(ierr);

          // ierr = MatView(Aij,PETSC_VIEWER_DRAW_SELF); CHKERRQ(ierr);
          // std::string wait;
          // std::cin >> wait;

          ierr = PetscPrintf(
            PETSC_COMM_WORLD,"number of Newton iterations = %D\n",its
          ); CHKERRQ(ierr);

          ierr = SNESGetConvergedReason(snes,&reason); CHKERRQ(ierr);

          if(reason<0) {
            t -= delta_time;
            delta_time *= 0.1;
            ierr = VecCopy(D0,D); CHKERRQ(ierr);
          } else {
            const int its_d = 6;
            const double gamma = 0.5;
            const double max_reudction = 1;
            const double min_reduction = 1e-1;
            double reduction;
            reduction = pow((double)its_d/(double)(its+1),gamma);
            if(delta_time >= max_reudction*delta_time0 && reduction > 1) {
              reduction = 1;
            } else if(delta_time <= min_reduction*delta_time0 && reduction < 1) {
              reduction = 1;
            }

            ierr = PetscPrintf(
              PETSC_COMM_WORLD,
              "reduction delta_time = %6.4e delta_time = %6.4e\n",
              reduction,delta_time
            ); CHKERRQ(ierr);
            delta_time *= reduction;
            if(reduction>1 && delta_time < min_reduction*delta_time0) {
              delta_time = min_reduction*delta_time0;
            }

            ierr = DMoFEMMeshToGlobalVector(
              dm,D,INSERT_VALUES,SCATTER_REVERSE
            ); CHKERRQ(ierr);
            ierr = DMoFEMLoopFiniteElements(
              dm,"PLASTIC",&small_strain_plasticity.feUpdate
            ); CHKERRQ(ierr);

            {
              double scale;
              ierr = time_force_scale.getForceScale(t,scale); CHKERRQ(ierr);
              NumeredDofEntity_multiIndex_uid_view_ordered::iterator it,hi_it;
              it = load_path_dofs_view.begin();
              hi_it = load_path_dofs_view.end();
              for(;it!=hi_it;it++) {
                PetscPrintf(PETSC_COMM_WORLD,
                  "load_path %s [ %d ] %6.4e %6.4e %6.4e\n",
                  (*it)->getName().c_str(),
//                  (*it)->getNbOfCoeffs(),
                  (*it)->getDofCoeffIdx(),
                  (*it)->getFieldData(),
                  t,scale
                );
              }
            }

            //Save data on mesh
            {
              ierr = DMoFEMLoopFiniteElements(
                dm,"PLASTIC",&post_proc
              ); CHKERRQ(ierr);
              string out_file_name;
              {
                std::ostringstream stm;
                stm << "out_" << step << ".h5m";
                out_file_name = stm.str();
              }
              ierr = PetscPrintf(
                PETSC_COMM_WORLD,"out file %s\n",out_file_name.c_str()
              ); CHKERRQ(ierr);
              rval = post_proc.postProcMesh.write_file(
                out_file_name.c_str(),"MOAB","PARALLEL=WRITE_PART"
              ); CHKERRQ_MOAB(rval);
            }

          }
        }

      }

      ierr = VecDestroy(&D0); CHKERRQ(ierr);

    }

    {
      ierr = MatDestroy(&Aij); CHKERRQ(ierr);
      ierr = VecDestroy(&F); CHKERRQ(ierr);
      ierr = VecDestroy(&D); CHKERRQ(ierr);
    }

  }

  PetscFinalize();
}
